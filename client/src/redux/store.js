import { configureStore } from '@reduxjs/toolkit'
import feedbackSlice from '../features/feedbackSlice'
export const store = configureStore({
  reducer: {
    feedback:feedbackSlice
  },
  
})