import { useState } from 'react'
import { Outlet } from "react-router-dom";
import './App.css'
import Header from './components/Header';
import Footer from './components/Footer';

function App() {
  

  return (
   <div>
    
    <Outlet/>
    
   </div>
  )
}

export default App
