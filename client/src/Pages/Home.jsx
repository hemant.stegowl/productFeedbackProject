import React,{useState,useEffect} from 'react'
import '../App.css'
import Feedback from '../components/feedback/Feedback'
import {Link} from 'react-router-dom'
import { useDispatch, useSelector } from "react-redux";
import { setSorting,fetchFeedback,setNewFilterdFeedback,updateUpvotes} from '../features/feedbackSlice';

function Home() {
  const dispatch=useDispatch();
  
  // const[newFilterdFeedback,setNewFilterdFeedback]=useState([])
  const newFilterdFeedback=useSelector((state)=>state.feedback.newFilterdFeedback)
console.log('✌️newFilterdFeedback --->', newFilterdFeedback);
    const feedbackData=useSelector((state)=>state.feedback.feedbackData)
    
console.log('✌️feedbackData --->', feedbackData);

    // useEffect(()=>{
    //     feedbackServices.getFeedback().then((res)=>{
    //         dispatch(setFeedbackData(res))
    //     })
    // },[])


    const sorting=useSelector((state)=>state.feedback.sorting)
    console.log('✌️sorting --->', sorting);

    const countComments = (feedback) => {
      let count = 0;
      feedback?.comments?.forEach((comment) => {
        count++; // Count the main comment
        if (comment?.replies && comment.replies.length > 0) {
          count += comment.replies.length;
        }
      });
      return count;
    };

    const handleSorting = (e) => {
      const sortingValue = e.target.value;
      dispatch(setSorting(sortingValue)); // Update sorting state in Redux
    
      // Sort feedbackData based on the selected sorting value
      // const sortedData = [...newFilterdFeedback].sort((a, b) => {
      //   if (sortingValue === 'most') {
      //     return b.upvotes - a.upvotes;
      //   } else {
      //     return a.upvotes - b.upvotes;
      //   }
        
      // });
      const sortedData = [...newFilterdFeedback].sort((a, b) => {
        if (sortingValue === 'most') {
          return b.upvotes - a.upvotes;
        } else if (sortingValue === 'least') {
          return a.upvotes - b.upvotes;
        } else if (sortingValue === 'most-comments') {
          return countComments(b) - countComments(a);
        } else if (sortingValue === 'least-comments') {
          return countComments(a) - countComments(b);
        } else {
          return b.upvotes - a.upvotes; // Default sorting
        }
      });
    
      // Update newFilteredFeedback state
      // setNewFilterdFeedback(sortedData);
      dispatch(setNewFilterdFeedback(sortedData))
    };

//     const sorting=useSelector((state)=>state.feedback.sorting)
//     console.log('✌️sorting --->', sorting);
  
//     const handleSorting=(e)=>{
//       dispatch(setSorting(e.target.value))
      
//     }




    const sortedFeedbackData = [...feedbackData].sort((a, b) => {
console.log('✌️just testing - inside sorted feedback data --->', );
      
      if (sorting === 'most') {
        return b.upvotes - a.upvotes;
      } 
      else if(sorting === 'least'){
        return a.upvotes - b.upvotes;
      }
      else if(sorting==='most-comments'){
        return countComments(b) - countComments(a); 

      }
      else {
        return countComments(a) - countComments(b); 
      }
    });
    
    
 
    useEffect(()=>{
       const feedback= dispatch(fetchFeedback())
console.log('✌️feedback --->', feedback.then((res)=>{
  console.log('✌️res --->', res.payload);

  const sortedFeedbackData = [...res.payload].sort((a, b) => {
  
    return b.upvotes - a.upvotes;
  } 
);
// setNewFilterdFeedback(sortedFeedbackData)
dispatch(setNewFilterdFeedback(sortedFeedbackData))
}));

       
    },[])


   // let sortedFeedbackData = [...feedbackData]; // Declare sortedFeedbackData outside of the conditional blocks
//console.log('✌️sortedFeedbackData --->', sortedFeedbackData);




  const handleFiltering=(category)=>{
    if(category=='all'){
      // setNewFilterdFeedback(sortedFeedbackData)
      dispatch(setNewFilterdFeedback(sortedFeedbackData))
      

    }
    else{
      const newFilterd=[...sortedFeedbackData].filter((one)=>{
        return one.category.toLowerCase()==category.toLowerCase()
      })
      console.log('✌️newFilterd --->', newFilterd);

      // setNewFilterdFeedback(newFilterd)
      dispatch( setNewFilterdFeedback(newFilterd))
    }
 

    
  }

 
// ====================
    // if (sorting === 'most') {
    //     console.log('most');
    //     sortedFeedbackData.sort((a, b) => b.upvotes - a.upvotes);
    //    // dispatch(setSortedFeedbackData2([...sortedFeedbackData].sort((a, b) => b.upvotes - a.upvotes)))
    //    dispatch(setSortedFeedbackData2([...sortedFeedbackData]))
    // } else {
    //     console.log('least');
    //     sortedFeedbackData.sort((a, b) => a.upvotes - b.upvotes);
    //    // dispatch(setSortedFeedbackData2([...sortedFeedbackData].sort((a, b) => a.upvotes - b.upvotes)))

    // }
    //const sortedFeedbackData=[...feedbackData].sort((a,b)=>( b.upvotes - a.upvotes))



  
//   const feedbackData=useSelector((state)=>state.feedback.feedbackData)
//   //const [feedbackData,setFeedbackData]=useState([])
// console.log('✌️feedbackData inside home --->', feedbackData);

//   // useEffect(()=>{
//   //     feedbackServices.getFeedback().then((res)=>{
//   //         dispatch(setFeedbackData(res))
//   //     })
//   // },[])
//   useEffect(()=>{
//       dispatch(fetchFeedback())
//   },[])


//   const [sorting,setSorting]=useState('most')
// console.log('✌️sorting --->', sorting);

  // const handleSorting=(e)=>{
  //   setSorting(e.target.value)
  // }
  



  const handleUpvotes=(id)=>{
    console.log('✌️id  inside upvotes handler--->', id);
            const obj=feedbackData.find(item=>item.id===id)
    console.log('✌️feedbackData  inside handle upvotes--->', feedbackData);
        console.log('✌️obj --->', obj);
            const newObj={...obj,upvotes:obj.upvotes+1}
        console.log('✌️newObj --->', newObj);
    
        dispatch(updateUpvotes({id,newObj})).then((res)=>{
            console.log('voting is done , inside then')
    
                const updatedUpvotesFeedback=newFilterdFeedback.map(item => {
                    if (item.id === id) {
                        return { ...item, upvotes: item.upvotes + 1 };
                    }
                    return item;
                })

                const sortedData = [...updatedUpvotesFeedback].sort((a, b) => {
                  if (sorting === 'most') {
                      return b.upvotes - a.upvotes;
                  } else {
                      return a.upvotes - b.upvotes;
                  }
              });

                dispatch(setNewFilterdFeedback(sortedData))
        })
        
    
            // feedbackServices.updateUpvotes(id,newObj).then((res)=>{
            //     console.log('updated the vote')
            //     // const updatedUpvotesFeedback=feedbackData.map(item => {
            //     //     if (item.id === id) {
            //     //         return { ...item, upvotes: item.upvotes + 1 };
            //     //     }
            //     //     return item;
            //     // })
            //     // dispatch(setFeedbackData(updatedUpvotesFeedback))
                
            //     const updatedUpvotesFeedback=props.newFilterdFeedback.map(item => {
            //         if (item.id === id) {
            //             return { ...item, upvotes: item.upvotes + 1 };
            //         }
            //         return item;
            //     })
            //     setNewFilterdFeedback(updatedUpvotesFeedback)
            // })
            
    
        }
  return (
    <div className='main'>
      <div className='left-sidebar'>
        <div className='logo '>
            <h3>Frontend Mentor</h3>
            <h4>Feedback Board</h4>
        </div>
        <div className='filter-option'>
            <button onClick={()=>{handleFiltering('all')}}>All</button>
            <button onClick={()=>{handleFiltering('ui')}}>UI</button>
            <button onClick={()=>{handleFiltering('ux')}}>UX</button>
            <button onClick={()=>{handleFiltering('enhancement')}}>Enhancement</button>
            <button onClick={()=>{handleFiltering('bug')}}>Bugs</button>
            <button onClick={()=>{handleFiltering('feature')}}>Features</button>

        </div>


       

<div class="roadmap-wrapper">
        <div class="roadmap-container">
            <div class="roadmap-header">
                <h1>Roadmap</h1>
                <Link to={'/roadmap'} class="view-link">View</Link>
            </div>
            <ul class="roadmap-list">
                <li>
                    <span class="dot planned"></span>
                    <span class="status">Planned</span>
                    <span class="count">2</span>
                </li>
                <li>
                    <span class="dot in-progress"></span>
                    <span class="status">In-Progress</span>
                    <span class="count">3</span>
                </li>
                <li>
                    <span class="dot live"></span>
                    <span class="status">Live</span>
                    <span class="count">1</span>
                </li>
            </ul>
        </div>
    </div>
      </div>
      <div className='right-sidebar'>
      {/* className='navbar' */}
            <div  className="roadmap-page-header">
                <h4>6 suggestions</h4>
                <p>
                  sortby: <select name="sort" id="sort" value={sorting} onChange={handleSorting}>
                              <option value="most">Most Upvotes</option>
                              <option value="least">Least Upvotes</option>
                              <option value="most-comments">Most Comments</option>
                              <option value="least-comments">Least Comments</option>
                          </select> 

                </p>
                <Link to={'/addFeedback'}><button className="feedback-button" >+ Add Feedback</button></Link> 

            </div>

            <div className='feedbackSection'>
              
              {/* <Feedback sorting={sorting} newFilterdFeedback={sortedFeedbackData}/> */}
              <Feedback sorting={sorting} newFilterdFeedback={newFilterdFeedback} setNewFilterdFeedback={setNewFilterdFeedback} handleUpvotes={handleUpvotes}/>
            </div>

       
      </div>
    </div>
  )
}

export default Home
