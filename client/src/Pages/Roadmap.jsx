import React,{useEffect} from 'react'
import { useParams,Link,useNavigate } from 'react-router-dom'
import { useDispatch,useSelector } from 'react-redux'
import { fetchFeedback,updateUpvotes,setFeedbackData } from '../features/feedbackSlice'



function Roadmap() {
    const navigate=useNavigate()
    const dispatch=useDispatch()

    const feedbackData=useSelector((status)=>status.feedback.feedbackData)
console.log('✌️feedbackData from roadmap --->', feedbackData);
    
  useEffect(()=>{
    dispatch(fetchFeedback())
  },[])

    const handlePrevious=(e)=>{
        e.preventDefault()
        navigate(-1)
      }

      const handleUpvotes = (id) => {
        const obj = feedbackData.find(item => item.id === id);
        console.log('✌️obj --->', obj);
      
        if (!obj) return; // Ensure the object exists
    
        const newObj = {...obj, upvotes: obj.upvotes + 1};
        console.log('✌️newObj --->', newObj);
    
        dispatch(updateUpvotes({ id, newObj }))
        .then((res) => {
            console.log('updated the vote', res);
            const updatedUpvotesFeedback = feedbackData.map(item => {
                if (item.id === id) {
                    return { ...item, upvotes: item.upvotes + 1 };
                }
                return item;
            });
            dispatch((updatedUpvotesFeedback));
        })
        .catch(error => {
            console.error('Failed to update upvotes:', error);
        });
    };

    //  filtering planned
    
    const planned=feedbackData.filter((one)=>{
        return one.status=='planned'
    })
    console.log('✌️planned --->', planned);

    const progress=feedbackData.filter((one)=>{
        return one.status=='in-progress'
    })
    console.log('✌️progress --->', progress);

    const live=feedbackData.filter((one)=>{
        return one.status=='live'
    })
    console.log('✌️live --->', live);


  return (
    <div className="roadmap-page-container">
    <div className="roadmap-page-header">
        <div>
        <button className='back-button-road' onClick={handlePrevious}>← Go Back</button>
        <h1>Roadmap</h1>

        </div>
        
       <Link to={'/addFeedback'}><button className="feedback-button" >+ Add Feedback</button></Link> 
    </div>
    <div className="roadmap-sections">
        <div className="roadmap-section">
            <h2>Planned ({planned.length})</h2>
            <p>Ideas prioritized for research</p>
            {planned?.map((one)=>{
                return(
                    <div className="card planned">
                <div className="card-header">
                    <span className="status-dot planned-dot"></span>
                    <h3>{one.title}</h3>
                </div>
                <p>{one.description}</p>
                <div className="tags">
                    <span className="tag">{one.category}</span>
                </div>
                <div className="feedback">
                    <button className="votes" onClick={()=>{handleUpvotes(one.id)}}>↑ {one.upvotes}</button>
                    <span className="comments">💬 2</span>
                </div>
            </div>
                )
            })}
            
           
        </div>
        <div className="roadmap-section">
            <h2>In-Progress ({progress.length})</h2>
            <p>Currently being developed</p>

            {progress?.map((one)=>{
                return(
                    <div className="card in-progress">
                <div className="card-header">
                    <span className="status-dot in-progress-dot"></span>
                    <h3>{one.title}</h3>
                </div>
                <p>{one.description}</p>
                <div className="tags">
                    <span className="tag">{one.category}</span>
                </div>
                <div className="feedback">
                    <span className="votes">↑ {one.upvotes}</span>
                    <span className="comments">💬 1</span>
                </div>
            </div>
                )
            })}
            
           
           
        </div>

        <div className="roadmap-section">
            <h2>Live ({live.length})</h2>
            <p>Released features</p>

            {live?.map((one)=>{
                return(
                    <div className="card live">
                <div className="card-header">
                    <span className="status-dot live-dot"></span>
                    <h3>{one.title}</h3>
                </div>
                <p>{one.description}</p>
                <div className="tags">
                    <span className="tag">{one.category}</span>
                </div>
                <div className="feedback">
                    <span className="votes">↑ {one.upvotes}</span>
                    <span className="comments">💬 2</span>
                </div>
            </div>
                )
            })}
            
        </div>
    </div>
</div>
  )
}

export default Roadmap
