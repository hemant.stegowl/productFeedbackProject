import React from 'react'
import ReactDOM from 'react-dom/client'
import App from './App.jsx'
import './index.css'
import { Route, RouterProvider, createBrowserRouter, createRoutesFromElements} from 'react-router-dom'
import Home from './Pages/Home.jsx'
import AddFeedback from './components/feedback/AddFeedback.jsx'
import FeedbackDetail from './components/FeedbackDetail.jsx'
import { store } from './redux/store.js'
import { Provider } from 'react-redux'
import EditFeedback from './components/feedback/EditFeedback.jsx'
import Roadmap from './Pages/Roadmap.jsx'

const router=createBrowserRouter(
  createRoutesFromElements(
   <>
    <Route path='/' element={<App />}>
      <Route path='' element={<Home/>}/>
      <Route path='/addFeedback' element={<AddFeedback/>}/>
      <Route path='/feedback/:id' element={<FeedbackDetail/>}/>
      <Route path='/editFeedback/:id' element={<EditFeedback/>}/>
      <Route path='/roadmap' element={<Roadmap/>}/>
     
    </Route>
   </>
   
  )
)

ReactDOM.createRoot(document.getElementById('root')).render(
  <React.StrictMode>
    <Provider store={store}>
     <RouterProvider router={router} />
    </Provider>
  </React.StrictMode>,
)
