import { createAsyncThunk,createSlice} from '@reduxjs/toolkit'
import axios from 'axios'

// export const fetchMovies=createAsyncThunk('fetchMovies',async()=>{
//     const response=await axios.get('https://dummyapi.online/api/movies?populate=*')
// console.log('✌️response --->', response.data);
//     return response.data    
// })
const baseUrl = 'http://localhost:3000/';

export const fetchFeedback=createAsyncThunk('fetchFeedback',async()=>{
    const response=await axios.get(`${baseUrl}productRequests`)
    return response.data
})
export const updateUpvotes=createAsyncThunk('updateUpvotes',async(params)=>{
    const {id,newObj}=params
console.log('✌️id inside slice --->', id);
console.log('✌️newObj --->', newObj);
    const response=await axios.patch(`${baseUrl}productRequests/${id}`,newObj)
    console.log('✌️response.data --->', response.data);
    return response.data
})

export const feedbackById=createAsyncThunk('feedbackById',async(id)=>{
    // const {id}=params;
    const response = await axios.get(`${baseUrl}productRequests/${id}`);
    return response.data;
})

export const updateFeedback=createAsyncThunk('updateFeedback',async(params)=>{
    const{id,newObj}=params;
    const response= await axios.patch(`${baseUrl}productRequests/${id}`,newObj)

    return response.data;
})

export const deleteFeedback=createAsyncThunk('deleteFeedback',async(id)=>{
    const response=await axios.delete(`${baseUrl}productRequests/${id}`)
    return response.data
})

export const addComment=createAsyncThunk('addComment',async(params)=>{
    const {id,commentObj,feedback,feedbackComment}=params
console.log('✌️comment --->', commentObj);

    // const response=await axios.patch(`${baseUrl}productRequests/${id}`, {...feedback,comments:[...comments,commentObj]})
    const response=await axios.put(`${baseUrl}productRequests/${id}`, feedbackComment)
      return response.data
})

export const addReply=createAsyncThunk('addReply',async (params)=>{
    const {id,repliedFeedbackObj}=params
    const response=await axios.put(`${baseUrl}productRequests/${id}`,repliedFeedbackObj)
    return response.data

})



export const feedbackSlice=createSlice({
    name:'feedback',
    initialState:{
        sorting:'most',
        feedbackData:[],
        sortedFeedbackData2:[],
        feedbackByIdData:{},
        newFilterdFeedback:[],
        numOfComments:0

       
    },
    reducers:{
        setSorting:(state,action)=>{
            state.sorting=action.payload
        },

        setFeedbackData:(state,action)=>{
            state.feedbackData=action.payload
        },

        setSortedFeedbackData2:(state,action)=>{
            state.sortedFeedbackData2=action.payload
        },

        setNewFilterdFeedback:(state,action)=>{
            state.newFilterdFeedback=action.payload

        },

        setNumOfComments:(state,action)=>{
            state.numOfComments=action.payload
        }


    },
    // extraReducers: (builder) => {
    //     builder
    //     .addCase(fetchMovies.pending, (state, action) => {
    //         console.log('pending data');
    //       })
    //       .addCase(fetchMovies.fulfilled, (state, action) => {
    //         // const arrAnecdote = action.payload.map(one => one.text);
    //         state.moviesData = action.payload;
    //       })        
    //       .addCase(fetchMovies.rejected, (state, action) => {
    //         console.log('error', action.payload);
    //       })
         
    //   }
    extraReducers:(builder)=>{
        builder
        .addCase(fetchFeedback.pending,(state,action)=>{
            console.log('pending data');
        })

        .addCase(fetchFeedback.fulfilled,(state,action)=>{
            state.feedbackData=action.payload;
        })

        .addCase(fetchFeedback.rejected,(state,action)=>{
            console.log('error',action.payload)
        })

        .addCase(feedbackById.fulfilled,(state,action)=>{
            state.feedbackByIdData=action.payload;
        })

        .addCase(updateUpvotes.fulfilled, (state, action) => {
            const updatedUpvoteObj = action.payload;
            // Map over the feedbackData array to find and update the specific feedback item
            const updatedFeedback = state.feedbackData.map(feedbackItem => {
                if (feedbackItem.id === updatedUpvoteObj.id) {
                    // If the id matches, return an updated object
                    return { ...feedbackItem, upvotes: updatedUpvoteObj.upvotes };
                }
                // Otherwise, return the item as is
                return feedbackItem;
            });
            console.log('✌️updatedFeedback in the slice--->', updatedFeedback);

        
            // Update the state's feedbackData with the new array
            state.feedbackData = updatedFeedback;

            // const updatedUpvotesFeedback=state.newFilterdFeedback.map(item => {
            //     if (item.id === updatedUpvoteObj.id) {
            //         return { ...item, upvotes: item.upvotes + 1 };
            //     }
            //     return item;
            // })
            // state.newFilterdFeedback=updatedUpvotesFeedback

           
        
            // Optionally log the update; might be removed in production
            console.log("Updated upvotes; if you want to vote more, press the upvotes again");
        })

        // .addCase(deleteFeedback.fulfilled,(state,action)=>{

        // })
        
        .addCase(addComment.fulfilled,(state,action)=>{
            console.log('you are seeing this log because comment is added . you won knight')
            const addedCommentFeedback=action.payload
console.log('✌️addedCommentFeedback --->', addedCommentFeedback); 
            const afeedbackData=state.feedbackData.map((item)=>{
                if(item.id==addedCommentFeedback.id){
                    return { ...item, comments: addedCommentFeedback.comments };
                }
                return item
            })
            console.log('✌️afeedbackData --->', afeedbackData);
            state.feedbackData=afeedbackData


        })

        .addCase(addComment.rejected,(state,action)=>{
            console.log('error',action.payload)
        })

        .addCase(addReply.fulfilled,(state,action)=>{
            console.log('you are seeing this log because comment is added . you won knight')
            const addedCommentFeedback=action.payload
                console.log('✌️addedCommentFeedback --->', addedCommentFeedback); 
            const afeedbackData=state.feedbackData.map((item)=>{
                if(item.id==addedCommentFeedback.id){
                    return { ...item, comments: addedCommentFeedback.comments };
                }
                return item
            })
            console.log('✌️afeedbackData --->', afeedbackData);
            state.feedbackData=afeedbackData

        })

       
    }
    
})
export const {setSorting,setFeedbackData,setSortedFeedbackData2,setNewFilterdFeedback,setNumOfComments}=feedbackSlice.actions;

export default feedbackSlice.reducer;
