import axios from 'axios';

// const baseUrl = 'http://localhost:3001/personsTest';
const baseUrl = 'http://localhost:3000/';

const getFeedback = async () => {
  const response = await axios.get(`${baseUrl}productRequests`);
  return response.data;
};

const createFeedback = async (newFeedback) => {
  const response = await axios.post(`${baseUrl}productRequests`, newFeedback);
  return response.data;
};

const updateUpvotes=async (id,newObj)=>{
  const response=await axios.put(`${baseUrl}productRequests/${id}`,newObj)
  return response.data;
}




const feedbackServices = {
  getFeedback,
  createFeedback,
  updateUpvotes
  
};

export default feedbackServices;




// const instance = axios.create({
//     baseURL: 'http://localhost:3000',
//     timeout: 5000,
//     headers: {
//       'Content-Type': 'application/json',
//     },
//   });

  // const fetchData=async (endpoint)=>{
  //   try{
  //       const response=await instance.get(endpoint)
  //       return response.data
  //   }
  //   catch(error){
  //       console.error(`Error fetching data from ${endpoint}:`, error);
  //       throw error;

  //   }
  // }

  // const postData=async (endpoint,newFeedback)=>{
  //   try{
  //       const response=await instance.post(endpoint,newFeedback)
  //       return response.data
  //   }
  //   catch(error){
  //       console.error(`Error creating data from ${endpoint}:`, error);
  //       throw error;

  //   }
  // }

  // const feedbackServices={
  //   getFeedback:()=>fetchData('/productRequests'), 
  //   createFeedback:(newFeedback)=>postData('/productRequests')
  // } 

  // export default feedbackServices;