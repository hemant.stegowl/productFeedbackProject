import React ,{useState,useEffect} from 'react'

function useField(initialValue) {
//  const initial=title
    // const [value, setValue] = useState(`${initial}`)
    const [value, setValue] = useState('')


    useEffect(() => {
        if (initialValue !== undefined) {
          setValue(initialValue);
        }
      }, [initialValue]);

    const onChange = (event) => {
      setValue(event.target.value)
    }
    return {
        
        value,
        onChange
      }
}

export default useField
