import React,{useState,useEffect} from 'react'
import { useParams,Link,useNavigate } from 'react-router-dom'
import { useDispatch,useSelector } from 'react-redux';
import { fetchFeedback,setFeedbackData,updateUpvotes,addComment, addReply,setNumOfComments } from '../features/feedbackSlice';
import useField from './hooks/useField';

function FeedbackDetail() {
  const navigate = useNavigate();
  const commentInput=useField();
  const replyInput=useField();

  const [showReply,setShowReply]=useState(false)
  // const [selectedCommentId, setSelectedCommentId] = useState(null);

  const [replyStates, setReplyStates] = useState({});

  const [numOfComments,setNewNumOfComments]=useState(0)

// Function to toggle reply component visibility for a specific comment
const toggleReply = (commentId) => {
  setReplyStates((prev) => ({
    ...prev,
    [commentId]: !prev[commentId],
  }));
};


   
  const dispatch=useDispatch()
  const feedbackData=useSelector((state)=>state.feedback.feedbackData)
 console.log('✌️feedbackData inside feedback detail --->', feedbackData);
 const {id}=useParams()
 console.log('✌️id --->', id);

 const feedback=feedbackData.find(one=>one.id==id)
console.log('✌️feedback --->', feedback);

  useEffect(()=>{
    dispatch(fetchFeedback())
  },[])

  const handleUpvotes = (id) => {
    const obj = feedbackData.find(item => item.id === id);
    console.log('✌️obj --->', obj);
  
    if (!obj) return; // Ensure the object exists

    const newObj = {...obj, upvotes: obj.upvotes + 1};
    console.log('✌️newObj --->', newObj);

    dispatch(updateUpvotes({ id, newObj })).then((res) => {
        console.log('updated the vote', res);
        const updatedUpvotesFeedback = feedbackData.map(item => {
            if (item.id === id) {
                return { ...item, upvotes: item.upvotes + 1 };
            }
            return item;
        });
        dispatch(setFeedbackData(updatedUpvotesFeedback));
    })
    .catch(error => {
        console.error('Failed to update upvotes:', error);
    });
};


//   const handleUpvotes=(id)=>{
//     const obj=feedbackData.find(item=>item.id===id)
//   console.log('✌️obj --->', obj);

//     const newObj={...obj,upvotes:obj.upvotes+1}
//   console.log('✌️newObj --->', newObj);

//     dispatch(updateUpvotes(id,newObj).then((res)=>{
//         console.log('updated the vote')
//         const updatedUpvotesFeedback=feedbackData.map(item => {
//             if (item.id === id) {
//                 return { ...item, upvotes: item.upvotes + 1 };
//             }
//             return item;
//         })
//         dispatch(setFeedbackData(updatedUpvotesFeedback))
//     }))
    

// }
const handlePrevious=(e)=>{
  navigate(-1);
}

// ====================handle adding comments============================
const handleComments=(id)=>{
console.log('✌️id inside handle comment --->', id);
  const commentId=Math.round(Math.random()*(1000 - 50 + 1)) + 50;
  const commentObj={
    id:commentId,
    user:{
      image: "./assets/user-images/image-ryan.jpg",
      name: "Ryan Welles",
      username: "voyager.344"
    },
    content:commentInput.value,
    replies:[]

  }
  
console.log('✌️feedback --->', feedback);
  const feedbackComment={
    ...feedback,
    comments: [...(feedback.comments || []), commentObj] 
  };
console.log('✌️feedbackComment --->', feedbackComment);

  dispatch(addComment({ id, commentObj,feedback,feedbackComment}))
  .then(() => {
    console.log('Comment added successfully');
  })
  .catch(error => {
    console.error('Error adding comment:', error);
    // Display an error message to the user
  });
}


// ==========================post reply=============

const handlePostReply=(params)=>{
  const {e,commentId,username}=params
console.log('✌️id --->', commentId);
  e.preventDefault()
  const replyObject= {
    content: replyInput.value,
    replyingTo: username,
    user: {
      image: "./assets/user-images/image-anne.jpg",
      name: "Anne Valentine",
      username: "annev1990"
    }
  }

  // const repliedFeedbackObj = {
  //   ...feedback,
  //   comments: feedback.comments.map(comment => {
     
  //       // Check if the comment has an existing replies array, if not, initialize it
        
  //       const replies = comment.replies || [];
  //       return {
  //         ...comment,
  //         replies: [...replies, replyObject]
  //       };
      
      
  //   })
  // };
  const repliedFeedbackObj = {
    ...feedback,
    comments: feedback?.comments?.map(comment => {
      if (comment?.id === commentId) {
        // Check if the comment exists and has the expected properties
        // If the comment is null or undefined, return it as is
        if (!comment) return comment;
  
        // Check if the comment has an existing replies array, if not, initialize it
        const replies = comment.replies || [];
        return {
          ...comment,
          replies: [...replies, replyObject]
        };
      }
      return comment; // Return the comment unchanged if it doesn't match the specified id
    })
  };
  

  // Example: Update the state or handle the updated feedback object appropriately
  
console.log('✌️repliedFeedbackObj --->', repliedFeedbackObj);
dispatch(addReply({id,repliedFeedbackObj}))


}

// =======================count the number of comments
// const countComments= ()=>{
//   let count=0;
//   feedback?.comments?.forEach((comment)=>{
//     count=+1;
//     if(comment && comment.replies){
//       count=+comment.replies.length
//     }

//   })
//   return count

// }
const countComments = () => {
  let count = 0;

  // Iterate through all comments
  feedback?.comments?.forEach((comment) => {
    count++; // Count the main comment

    // If the comment has replies, add their length to the count
    if (comment?.replies && comment.replies.length > 0) {
      count += comment.replies.length;
    }
  });

  return count;
};

const newNumOfComments= countComments()
//dispatch(setNumOfComments(newNumOfComments))
console.log('✌️numOfComments --->', newNumOfComments);
// setNewNumOfComments(newNumOfComments)



  return (
   <div className='main-FeedbackDetail-outer'>
     <div className='main-FeedbackDetail'>
      <div className='feedbackDetail-container-top'>
        <button className='back-button' onClick={handlePrevious}>Go Back</button>
        <Link to={`/editFeedback/${feedback?.id}`}><button>edit feedback</button></Link>
      </div>
    
      <div className='feedbackCard'>
                <div className='card-item'>
                   <button onClick={()=>{handleUpvotes(feedback.id)}}>{feedback?.upvotes}</button> 
                </div>
                
                    <div className='card-mid'>
                        <h4>{feedback?.title}</h4>
                        <p>{feedback?.description}</p>
                        <p>{feedback?.category}</p>
                    </div>
                

                <div className='comment-section'>
                    <h3>{newNumOfComments}</h3>
                </div>
     
            </div>

      <div className='comments-container'>
        
          <h3>Comments</h3>

          {feedback?.comments?.map((one)=>{
            console.log('✌️one --->', one);
            const commentId = one?.id;
            const username=one?.user?.username
              return (
                
                     <div className="comment">
                     <div className="user-info">
                       <img src="user-image.jpg" alt="User Avatar" className="avatar"/>
                       <div className="user-details">
                        <span className="username">{one?.user?.name}</span>
                         <span className="user-id">{one?.user?.username}</span>
                       </div>
                       <button onClick={() => toggleReply(commentId)}>reply</button>
                     </div>
                     <p className="comment-text">{one?.content}</p>


                     {replyStates[commentId] &&
                      <div className='replyPost'>
                      <form action="#">
                        <input type="text" value={replyInput.value} onChange={replyInput.onChange} placeholder='write your reply' />
                        <button onClick={(e)=>{handlePostReply({e,commentId,username})}}>post reply</button>
                      </form>
                    </div>
                     }

                     <h5>replies</h5>
                     {one?.replies?.map((reply)=>{
                      return (
                        <div className="comment-replies">
                     <div className="user-info">
                       <img src="user-image.jpg" alt="User Avatar" className="avatar"/>
                       <div className="user-details">
                        <span className="username">{reply?.user?.name}</span>
                         <span className="user-id">{reply?.user?.username}</span>
                       </div>
                       {/* <button onClick={() => toggleReply(commentId)}>reply</button> */}
                       {/* <button >reply</button> */}
                     </div>
                     <p className="comment-text">{reply?.replyingTo}: {reply?.content}</p>
                     </div>

                      )
                      
                     })}
                    
                     {/* {showReply && one.replies.map((item)=>{
                        return (
                          <div>
                            this is reply
                          </div>


                        )
                     })} */}
                  
               
         
            </div>
              )
          })}
         
   
      </div>


      <div className='add-comment'>
        <h4>add comments</h4>
        <input type="text" value={commentInput.value} onChange={commentInput.onChange} />
        <div className='add-comment-bottom'>
          <h5>something left</h5>
          <button onClick={()=>handleComments(feedback.id)}>post</button>
        </div>
      </div>
    </div>
   </div>
  )
}

export default FeedbackDetail
