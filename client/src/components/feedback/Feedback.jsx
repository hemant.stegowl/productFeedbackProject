import React,{useState,useEffect} from 'react'
import '../../App.css'
import feedbackServices from '../../services/feedbackService'
import { Link } from 'react-router-dom'
import { useDispatch, useSelector } from "react-redux";
import { setSorting,setFeedbackData,fetchFeedback,setSortedFeedbackData2, updateUpvotes,setNewFilterdFeedback} from '../../features/feedbackSlice';

function Feedback(props) {
    const dispatch=useDispatch();
    const handleUpvotes=props.handleUpvotes
    // const setNewFilterdFeedback=props.setNewFilterdFeedback;
    const newFilterdFeedback=useSelector((state)=>state.feedback.newFilterdFeedback)
//=======================================
//     const sorting=useSelector((state)=>state.feedback.sorting)
     const feedbackData=useSelector((state)=>state.feedback.feedbackData)

     const numOfComments=useSelector((state)=>state.feedback.numOfComments)
//     //const [feedbackData,setFeedbackData]=useState([])
// console.log('✌️feedbackData --->', feedbackData);

//     // useEffect(()=>{
//     //     feedbackServices.getFeedback().then((res)=>{
//     //         dispatch(setFeedbackData(res))
//     //     })
//     // },[])
//     useEffect(()=>{
//         dispatch(fetchFeedback())
//     },[])

//    // let sortedFeedbackData = [...feedbackData]; // Declare sortedFeedbackData outside of the conditional blocks
// //console.log('✌️sortedFeedbackData --->', sortedFeedbackData);

//   const sortedFeedbackData = [...feedbackData].sort((a, b) => {
//     if (sorting === 'most') {
//       return b.upvotes - a.upvotes;
//     } else {
//       return a.upvotes - b.upvotes;
//     }
//   });
// =========================================================
 

    // if (sorting === 'most') {
    //     console.log('most');
    //     sortedFeedbackData.sort((a, b) => b.upvotes - a.upvotes);
    //    // dispatch(setSortedFeedbackData2([...sortedFeedbackData].sort((a, b) => b.upvotes - a.upvotes)))
    //    dispatch(setSortedFeedbackData2([...sortedFeedbackData]))
    // } else {
    //     console.log('least');
    //     sortedFeedbackData.sort((a, b) => a.upvotes - b.upvotes);
    //    // dispatch(setSortedFeedbackData2([...sortedFeedbackData].sort((a, b) => a.upvotes - b.upvotes)))

    // }
    //const sortedFeedbackData=[...feedbackData].sort((a,b)=>( b.upvotes - a.upvotes))

//     const handleUpvotes=(id)=>{
// console.log('✌️id  inside upvotes handler--->', id);
//         const obj=feedbackData.find(item=>item.id===id)
// console.log('✌️feedbackData  inside handle upvotes--->', feedbackData);
//     console.log('✌️obj --->', obj);
//         const newObj={...obj,upvotes:obj.upvotes+1}
//     console.log('✌️newObj --->', newObj);

//     dispatch(updateUpvotes({id,newObj})).then((res)=>{
//         console.log('voting is done , inside then')

//             const updatedUpvotesFeedback=newFilterdFeedback.map(item => {
//                 if (item.id === id) {
//                     return { ...item, upvotes: item.upvotes + 1 };
//                 }
//                 return item;
//             })
//             dispatch(setNewFilterdFeedback(updatedUpvotesFeedback))
//     })
    

//         // feedbackServices.updateUpvotes(id,newObj).then((res)=>{
//         //     console.log('updated the vote')
//         //     // const updatedUpvotesFeedback=feedbackData.map(item => {
//         //     //     if (item.id === id) {
//         //     //         return { ...item, upvotes: item.upvotes + 1 };
//         //     //     }
//         //     //     return item;
//         //     // })
//         //     // dispatch(setFeedbackData(updatedUpvotesFeedback))
            
//         //     const updatedUpvotesFeedback=props.newFilterdFeedback.map(item => {
//         //         if (item.id === id) {
//         //             return { ...item, upvotes: item.upvotes + 1 };
//         //         }
//         //         return item;
//         //     })
//         //     setNewFilterdFeedback(updatedUpvotesFeedback)
//         // })
        

//     }

    const countComments = (feedback) => {
        let count = 0;
      
        // Iterate through all comments
        feedback?.comments?.forEach((comment) => {
          count++; // Count the main comment
      
          // If the comment has replies, add their length to the count
          if (comment?.replies && comment.replies.length > 0) {
            count += comment.replies.length;
          }
        });
      
        return count;
      };
      
    //   const newNumOfComments= countComments()
    //   //dispatch(setNumOfComments(newNumOfComments))
    //   console.log('✌️numOfComments --->', newNumOfComments);

    
  return (
    
    <div>
    {newFilterdFeedback?.map((feedback)=>{
        const newNumOfComments = countComments(feedback);
        return(
           
             <div className='feedbackCard' key={feedback.id}>
                <div className='card-item'>
                   <button onClick={()=>{handleUpvotes(feedback.id)}}>{feedback.upvotes}</button> 
                </div>
                <Link to={`feedback/${feedback.id}`} style={{ textDecoration: 'none' }}> 
                    <div className='card-mid'>
                        <h4>{feedback.title}</h4>
                        <p>{feedback.description}</p>
                        <div className='category-text'>{feedback.category}</div>
                    </div>
                </Link>  

                 <div className='card-item'>
                   {newNumOfComments}
                </div>  

                {/* <div className='card-item'>
                    comment
                </div> */}
     
            </div>

        )
    })}
        
    </div>
    
  )
}

export default Feedback
