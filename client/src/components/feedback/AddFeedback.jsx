import React ,{useState,useEffect}from 'react'
import feedbackServices from '../../services/feedbackService';
import { useNavigate } from 'react-router-dom';

import '../../Creation.css'





function AddFeedback() {
  const navigate = useNavigate();

  const[title, setTitle]=useState('')
console.log('✌️title --->', title);
  const[category, setCategory]=useState('')
console.log('✌️category --->', category);
  const[detail, setDetail]=useState('')
console.log('✌️detail --->', detail);

   const [comments,setComments]=useState([])


  const handleTitle=(event)=>{
    setTitle(event.target.value)

  }
  const handleCategoryChange=(event)=>{
    setCategory(event.target.value)
  }

  const handleDetail=(event)=>{
    setDetail(event.target.value)
  }

  const handleSubmit=(event)=>{
    event.preventDefault();
    const newFeedback={
      title:title,
      category:category,
      upvotes:0,
      comments:comments,
      description:detail,
      status: "suggestion"
    }
    console.log('✌️newFeedback --->', newFeedback);
    feedbackServices.createFeedback(newFeedback).then(()=>{
      console.log('created a feedback')
    })

  }
  // return (
  //     <div className='add-feedback-form-body'>
       
  //       give your valuable feedback here
  //     <div>

  //     </div>

     
  //     <form onSubmit={handleSubmit} className='adding-form'>
  //      <h3> Create New Feedback</h3>
       
  //           <label htmlFor="title">feedback title</label>
  //       <div className='adding-form-input'> 
  //           <input type="text" name='title' id='title' value={title} onChange={handleTitle} />
  //       </div>
        

       
  //      <label htmlFor="category">category</label>
  //      <div className='adding-form-input'> 
  //       <select name="category" id="category" value={category} onChange={handleCategoryChange}>
  //         <option value="all">All</option>
  //         <option value="ui">UI</option>
  //         <option value="ux">Ux</option>
  //         <option value="features">Features</option>
  //         <option value="enhancement">Enhancement</option>
  //         <option value="bug">Bug</option>


  //       </select>
  //      </div>
  //       {/* <input type="text" value={category} onChange={handleCategory}/> */}


  //       <label htmlFor="detail">feedback detail</label>
  //       <div className='adding-form-input'> 
  //           {/* <input type="" value={detail} onChange={handleDetail} /> */}
  //           <textarea id="detail" name="detail" rows="4" cols="50" value={detail} onChange={handleDetail} ></textarea>
  //       </div>
  //       <button type='submit'>create feedback</button>




  //     </form>
      
    
  //   </div>
    
  // )

  const handlePrevious=(e)=>{
    e.preventDefault()
    navigate(-1)
  }

  return (
    <div>
      <button className='back-button' onClick={handlePrevious}>Go Back</button>
<div className='edit-main'>
      
      <div className='add-container'>
      <h1>Create New Feedback </h1>
    <form action="#" onSubmit={handleSubmit}>
      <div className="form-group">
        <label htmlFor="title">Feedback Title</label>
        <span>Add a short, descriptive title</span>
         <input type="text" name='title' id='title' className='form-controls' value={title} onChange={handleTitle} />
      </div>

      <div className="form-group">
      <label htmlFor="category">category</label>
      <span>Choose a category for your feedback</span>
       <select name="category" id="category" value={category} onChange={handleCategoryChange}>
          <option value="all">All</option>
          <option value="ui">UI</option>
          <option value="ux">Ux</option>
          <option value="features">Features</option>
          <option value="enhancement">Enhancement</option>
          <option value="bug">Bug</option>


        </select>
      </div>



      <div className='form-group'> 
      <label htmlFor="detail">feedback detail</label>
      <span>include any comments on what should be improoved</span>
      
          {/* <input type="" value={detail} onChange={handleDetail} /> */}
          <textarea id="detail" name="detail" rows="4" className='message' cols="50" value={detail} onChange={handleDetail} ></textarea>
      </div>
      <div>
          <button type='submit'>create feedback</button>


      </div>
    </form>
  </div>
  </div>
    </div>
   
  )
}

export default AddFeedback
