import React, {useEffect,useState} from 'react'
import '../../App.css'
import { useParams,Link,useNavigate } from 'react-router-dom'
import { useSelector,useDispatch } from 'react-redux'
import { fetchFeedback,feedbackById,updateFeedback ,deleteFeedback} from '../../features/feedbackSlice'
import useField from '../hooks/useField'

function EditFeedback() {
  const navigate = useNavigate();
 
    const dispatch=useDispatch()
//     const feedbackData=useSelector((state)=>state.feedback.feedbackData)
// console.log('✌️feedbackData --->', feedbackData);
    const{ id}=useParams()
console.log('✌️id --->', id);

  const feedback=useSelector((state)=>state.feedback.feedbackByIdData)
console.log('✌️feedback --->', feedback);


useEffect(()=>{
    // const feedback= dispatch(feedbackById())
    dispatch(feedbackById(id))
console.log('✌️dispatch just bcozz--->', );
    


},[]);

const title = useField(feedback?.title)
console.log('✌️feedback.title --->', feedback.title);

const category=useField(feedback.category)

const detail=useField(feedback.description)

const status= useField(feedback?.status)


const handleUpdate=(e)=>{
  e.preventDefault()
  
  console.log('✌️id  inside handler--->', id);
  console.log('✌️title inside of the handler --->', title);

  const newObj={
    title: title.value,
    category: category.value,
    status: status.value,
    description: detail.value
  }

  console.log('✌️newObj --->', newObj);

  dispatch(updateFeedback({id,newObj}))
}

const handleDelete=(e)=>{
  e.preventDefault();
  dispatch(deleteFeedback(id))
  navigate(-2)

}

const handlePrevious=(e)=>{
  e.preventDefault()
  navigate(-1)
}

  return (
    <div>
      <button className='back-button' onClick={handlePrevious}>Go Back</button>
<div className='edit-main'>
      
      <div className='edit-container'>
      <h1>Editing {`'${feedback.title}'`} </h1>
    <form action="#">
      <div className="form-group">
        <label htmlFor="title">Feedback Title</label>
        <span>Add a short, descriptive title</span>
        <input type="text"  id='title' name='title' className='form-controls' value={title.value} onChange={title.onChange} />  
      </div>

      <div className="form-group">
      <label htmlFor="category">category</label>
      <span>Choose a category for your feedback</span>
      <select name="category" id="category" value={category.value} onChange={category.onChange} >
        <option value="all">All</option>
        <option value="ui">UI</option>
        <option value="ux">Ux</option>
        <option value="features">Features</option>
        <option value="enhancement">Enhancement</option>
        <option value="bug">Bug</option>


      </select>
      </div>

      <div className="form-group">
        <label htmlFor="status">Upadate Status</label>
        <span>change feedback status </span>
        {/* <input type="text"  id='status' name='status' className='form-controls' />   */}
       <select name="status" id="status" value={status.value} onChange={status.onChange}  >
        <option value="Suggestions">Suggestions</option>
        <option value="Planned">Planned</option>
        <option value="in-progress">In-Progress</option>
        <option value="live">Live</option>
      </select>
      </div>

      <div className='form-group'> 
      <label htmlFor="detail">feedback detail</label>
      <span>include any comments on what should be improoved</span>
      
          {/* <input type="" value={detail} onChange={handleDetail} /> */}
          <textarea id="detail" name="detail" className='message' value={detail.value}  onChange={detail.onChange}></textarea>
      </div>
      <div>
        <button onClick={handleDelete}>Delete</button>
        <button>Cancel</button>

        <button onClick={handleUpdate}>Add Feedback</button>

      </div>
    </form>
  </div>
  </div>
    </div>
   
  )
}

export default EditFeedback
